# Adapt

### USAGE:
    ADAPT adaptee prelude

### DESCRIPTION:
    Create a variant of an ACTION! that preprocesses its arguments  
    ADAPT is an ACTION!

### RETURNS: [action!]

### ARGUMENTS:

    adaptee [action! word! path!]  
        Function or specifying word (preserves word for debug info)  
    prelude [block!]  
        Code to run in constructed frame before adaptee runs
        
        