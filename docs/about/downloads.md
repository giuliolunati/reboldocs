# Downloads

## Rebol3 Alpha

[Rebolsource](http://www.rebolsource.net)

## Ren-c stable

Use these binaries for stable scripting

* Android
* Linux 32
* Linux 64
* iOS 64
* Windows 64

:construction_worker:

## Ren-c latest (experimental)

These are downloaded from a rebol console in browser!

Go to the [console](http://hostilefork.com/media/shared/replpad-js/)

and once it has loaded, type

```
do <downloads>
```

Then click on which platform you would like.
